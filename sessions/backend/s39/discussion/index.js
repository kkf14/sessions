console.log(fetch('https://jsonplaceholder.typicode.com/posts'));


fetch('https://jsonplaceholder.typicode.com/posts')
.then((response, error) => console.log(response));


// fetch('https://jsonplaceholder.typicode.com/posts')
// 	.then(response => response.json()) // converts the JSON string from the response into regular javascript format
// 	.then(posts => console.log(posts));

// async function fetchData(){
// 	let result = await fetch('https://jsonplaceholder.typicode.com/posts');

// 	let json_result = await result.json();

// 	console.log(json_result);
// }

// fetchData();	

// // Adding headers, body, and method to the fetch() function
// fetch('https://jsonplaceholder.typicode.com/posts', {
// 	method: 'POST',
// 	headers: {
// 		'Content-Type': 'application/json'
// 	},
// 	body: JSON.stringify({
// 		title: "New post!",
// 		body: "Hellow World.",
// 		userId: 2
// 	})
// })
// .then(response => response.json())
// .then(created_post => console.log(created_post));


//  // Updating existing posts
// fetch('https://jsonplaceholder.typicode.com/posts/1', {
// 	method: 'PUT',
// 	headers: {
// 		'Content-Type': 'application/json'
// 	},
// 	body: JSON.stringify({
// 		title: "Corrected Post!"

// 	})
// })
// .then(response => response.json())
// .then(updated_post => console.log(updated_post));

// //Deleting existing post
// fetch('https://jsonplaceholder.typicode.com/posts/1', {
// 	method: 'DELETE'
// })
// .then(response => response.json())
// .then(deleted_post => console.log(deleted_post));

// // Filtering Posts
// fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
// .then(response => response.json())
// .then(post => console.log(post));

// // Getting comments of a post
// fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
// .then(response => response.json())
// .then(post => console.log(post));