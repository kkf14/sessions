// Server variables
const express = require('express');
const mongoose = require('mongoose')
require('dotenv').config(); // Initialization of dotenv package
const app = express();
const port = 4000;

// MongoDB Connection	
mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@303-fulcher.0hsltfc.mongodb.net/b303-todo-db?retryWrites=true&w=majority`, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.on('error', () => console.log('Connection error :('));
mongoose.connection.once('open', () => console.log('Connected to MongoDB!'));

// Middleware

app.use(express.json());
app.use(express.urlencoded({extended: true}));

// [SECTION] Mongoose Schema
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});

// [SECTION] Models
const Task = mongoose.model("Task", taskSchema);

// Creating a new task
// [SECTION] Routes


app.post('/tasks', (request, response) => {
	Task.findOne({name: request.body.name}).then((result, error) => {
		// Checks if the task already exists by utilizingthe 'name' property. If it does, then return a response to the user.
		
		if(result != null && result == "" && result.name == request.body.name){
			return response.send("Duplicate task found!");
		} else {
			// 1.) Create a new instance of the task model which will contain the properties required based on the schema
			let newTask = new Task({
				name: request.body.name
			});

			// 2.) Save the new task to the database
			newTask.save().then((savedTask, error) => {
				if(error){
					return response.send({
						message: error.message
					})
				}

				return response.send(201, "New task created!");
			})
		}
	})
})

app.get('/tasks', (request, response) => {
	Task.find({}).then((result, error) => {
		if(error){
			return response.send({
				message: error.message
			})
		}

		return response.status(200).json({
			tasks: result
		})
	})
})

// Server Listening
app.listen(port, () => console.log(`Server is running at localhost: ${port}`));

module.exports = app;