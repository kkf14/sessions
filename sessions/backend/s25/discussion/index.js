console.log("Hi");

// [SECTION] Objects
/*
	-An object is a data type that is used to represent real world objects
	-Create properties and methods/functionalities
*/

// Creating objects using initializers/ object literals {}
let cellphone = {
	// key:value pair
	name: "Nokia 3210",
	manufatureDate: 1999
};

console.log("Result from creating objects using intializers/object literal");
console.log(cellphone);
console.log(typeof cellphone);

// Creating objects using a contructor function
function Laptop(name, manufatureDate) {
	this.name = name;
	this.manufatureDate = manufatureDate;
}

// Multiple instance of an object using the "new" keyword
// This method is called instantiation | Instance of an object
let laptop = new Laptop('Lenovo', 2008);
console.log("Result from creating objects using contructor function");
console.log(laptop);

let laptop2 = new Laptop('Macbook Air', 2020);
console.log("Result from creating objects using contructor function");
console.log(laptop2);

// [SECTION] Accessing Object Properties

// Using square bracket notation    object["property"]
console.log("Result from square bracket notation: " + laptop2['name']);
// Using dot (.) notation	object.property
console.log("Result from dot notation: " + laptop2.name);

// Access array objects
let array = [laptop, laptop2];

console.log(array[0]['name']);
console.log(array[0].name);

// [SECTION] Adding/Deleting/Reassigning Object Properties

// empty object using object literals
let car = {};

// empty object using contructor function
let myCar = new Object();
console.log(myCar);

// ---adding object properties---
// objectName.key = "value"
car.name = "Honda Civic";
console.log("Result from adding properties using dot notation: ");
console.log(car);

// adding object properties using square bracket notation
car["manufacturing date"] = 2019;
console.log(car["manufacturing date"]);
// square bracket is case sensitive
console.log(car["Manufacturing Date"]);
// we cannot access the object using dot notation if the key has spaces
//console.log(car.manufacturing date);
console.log("Result fro madding properties using square bracket notation:");
console.log(car);

// ---deleting object properties---
delete car["manufacturing date"];
console.log("Result from deleting properties: ");
console.log(car);


// ---reassigning object properties---
// property names are case sensitive
car.Name = "Honda Civic Type R";
console.log("Result from reassigning properties: ");
console.log(car);

// [SECTION] Object Methods
/*
	- a method is a function which acts as a property of an object
*/

let person = {
	name: "Barbie",
	greet: function() {
		console.log("Hello! My name is " + this.name);
	}
}

person.greet();

// adding walking method to object
person.walk = function(){
	console.log(this.name + " Walked 25 steps forward");
}

person.walk();



let friend = {
	name: "Ken",
	address: {
		city: "Austin",
		state: "Texas",
		country: "USA"
	},
	email: ["ken@gmail.com", "ken@mail.com"],
	introduce: function(person) {
		console.log("Nice to meet you " + person.name + " I am " + this.name + " from " + this.address.city + " " + this.address.state);
	}
}

friend.introduce(person);