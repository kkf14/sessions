let http = require("http");

// create a variable port to store the port number: 4000
const port = 4000;

// create avariable app that store the output of the createServer() method
// this allows us to use th ehttp createServer's other methods
const app = http.createServer((req, res) => {

	if(req.url == '/greeting') {
		res.writeHead(200, {'Content-Type': 'text/plain'});

		res.end('Hello Again');

	} else if(req.url == '/homepage') {

		res.writeHead(200, {'Content-Type': 'text/plain'});

		res.end('Welcome to the homepage.');

	} else {

		res.writeHead(200, {'Content-Type': 'text/plain'});

		res.end('Page not available.');
	} 
})

app.listen(port);

console.log(`Server now accessible at localhost:${port}.`);