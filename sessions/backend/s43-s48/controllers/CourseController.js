const Course = require('../models/Course.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

module.exports.addCourse = (request, response) => {
	let new_course = new Course({
		name: request.body.name,
		description: request.body.description,
		price: request.body.price
	});

	return new_course.save().then((saved_course, error) => {

		console.log("Hello from new course");
		if(error) {
			return response.send(false);
		}

		return response.send(true);
	}).catch(error => console.log(error));
}

module.exports.getAllCourses = (request, response) => {
	return Course.find({}).then(result => {
		return response.send(result);
	})
}

module.exports.getAllActiveCourses = (request, response) => {
	return Course.find({isActive: true}).then(result => {
		return response.send(result);
	})
}

module.exports.getCourse = (request, response) => {
	return Course.findById(request.params.id).then(result => {
		//console.log(result);
		return response.send(result);
	})
}

module.exports.updateCourse = (request, response) => {
	let updated_course_detail = {
		name: request.body.name,
		description: request.body.description,
		price: request.body.price
	};

	return Course.findByIdAndUpdate(request.params.id, updated_course_detail).then ((course, error) => {
		if(error) {
			return response.send({
				message: error.message
			})
		}

		return response.send({
			message: 'Course has been updated successfully!'
		})
	})
}

////////////////////////////////
module.exports.archiveCourse = (request, response) => {
	return Course.findByIdAndUpdate(request.params.id, {isActive: false}).then ((course, error) => {
		if(error) {
			return response.send(false)
		}

		return response.send(true)
	})
}

module.exports.activateCourse = (request, response) => {
	return Course.findByIdAndUpdate(request.params.id, {isActive: true}).then ((course, error) => {
		if(error) {
			return response.send(false)
		}

		return response.send(true)
	})
}

module.exports.searchCourses = (request, response) => {
  
    const courseName = request.body.name;
    Course.find({ name: { $regex: courseName, $options: 'i' } }).then((courses) => {
    	response.send(courses)
    }).catch(error => response.send({
    	message: error.message
    }))
 }

 module.exports.deleteCourse = (request, response) => {
	Course.findByIdAndRemove(request.params.id).then((deletedCourse) => {
      if (!deletedCourse) {
        return response.status(404).json({ message: 'Course not found' });
      }

      return response.status(200).json({ message: 'Course deleted successfully' });
    })
    .catch((error) => {
      console.error(error);
      return response.status(500).json({ message: 'Internal server error' });
    });
};