const express = require('express');
const router = express.Router();
const CourseController = require('../controllers/CourseController.js');
const auth = require('../auth.js');
const {verify, verifyAdmin} = auth;
// You can destructure the 'auth variable to extract the fucntion being exported from it. You can then use the function directly without having to use the dor (.) notation'


// Insert routes here

// Create single post
router.post('/', auth.verify, auth.verifyAdmin, (request, response) => {
	CourseController.addCourse(request, response)
});

// Get all courses
router.get('/all', (request, response) => {
	CourseController.getAllCourses(request, response);
});

// Get all active courses
router.get('/', (request, response) => {
	CourseController.getAllActiveCourses(request, response);
});

// Get single course
router.get('/:id', (request, response) => {
	CourseController.getCourse(request, response);
});

// Update course
router.put('/:id', auth.verify, auth.verifyAdmin, (request, response) => {
	CourseController.updateCourse(request, response);
})

// Archiving Course //////////
router.put('/:id/archive', auth.verify, (request, response) => {
	CourseController.archiveCourse(request, response);
})

// Activating Course
router.put("/:id/activate", auth.verify, auth.verifyAdmin, (request, response) => {
    CourseController.activateCourse(request, response);
 });

// Search course by name
router.post('/search', (request, response) => {
	CourseController.searchCourses(request, response);
});

// Delete Course
router.delete('/:id/delete', auth.verify, auth.verifyAdmin, (request, response) => {
  CourseController.deleteCourse(request, response);
});

module.exports = router;