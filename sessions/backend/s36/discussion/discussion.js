// Using the aggregate method

// Output the total amount fruits of according to their supplier id
db.fruits.aggregate([
	// $match - used to match or get documents that satisfies the conition
	// $match is similar to find(). You can use query operators to make your
	// criteria more flexiple
	/*
		$match -> Apple, Kiwi, Banana
	*/
	{ $match: { onSale: true }},
	// $group - Allows us to group together documents and create an analysis output of
	// the group elements
	// _id: $supplier id
	/*
		Apple = 1.0
		Kiwi = 1.0
		Banana = 1.0

		_id: 1.0
			Apple, Kiwi

			total: Sum of the fruit stock of 1.0
			total: Apple Stocks + Kiwi Stocks
			total: 20 + 25
			total: 45

		_id: 2.0
			Banana, Mango

			total: Sum of the fruit stock of 2.0
			total: Banana Stocks 
			total: 15
	*/
	// $sum - used to add or total the values in a given field
 	{ $group: { _id: "$supplier_id", total: {$sum: "$stock"}}}
]);


db.fruits.aggregate([
	/*
		$match -> Apple, Kiwi, Banana
	*/
	{ $match: { onSale:true}},
	/*
		Apple = 1.0
		Kiwi = 1.0
		Banana = 1.0

		_id: 1.0
			
			avgStocks: Average stocks of fruits in 1.0
			avgStocks: (Apple Stocks + Kiwi Stocks) / 2
			avgStocks: (20 + 25) / 2
			avgStocks: 22.5

		_id: 2.0

			avgStocks: Average stocks of fruits in 2.0
			avgStocks: Banana Stocks / 1
			avgStocks: 15 / 1
			avgStocks: 15
			
	*/
	/*
		{
			_id: 1,
			avgStocks: 22.5
		},
		{
			_id: 2,
			avgStocks: 15
		}

	*/
	// $avg - gets the average of the values of the given field per group
	{ $group: { _id: "$supplier_id", avgStocks: { $avg: "$stock" }}},
	// $project - can be used when aggregating data to include/exclude fields
	// from the returned results
	{ $project: { _id: 0 }}
]);

db.fruits.aggregate([
	/*
		$match -> Apple, Kiwi, Banana
	*/
	{ $match: { onSale: true }},
	/*
		Apple = 1.0
		Kiwi = 1.0
		Banana = 1.0

		_id: 1.0
			
			maxPrice: Finds the highest price of fruit in 1.0
			maxPrice: Apple Price VS Kiwi Price
			maxPrice: 40 VS 50
			maxPrice: 50

		_id: 2.0

			maxPrice: Finds the highest price of fruit in 2.0
			maxPrice: Banana Price 
			maxPrice: 20
			maxPrice: 20
			
	*/
	/*
		{
			_id: 1,
			maxPrice: 50
		},
		{
			_id: 2,
			maxPrice: 20
		}

	*/
 	{ $group: { _id: "$supplier_id", maxPrice: { $max: "$price"}}},
 	// $sort - Chanhes the order of the aggregated result
 	// -1 Reverse Order
    { $sort: { maxPrice: -1 }}
]);


db.fruits.aggregate([
	// $unwind - deconstructs the array field from a collection with an array value
	// to output result for each element
	{ $unwind: "$origin"}
]);