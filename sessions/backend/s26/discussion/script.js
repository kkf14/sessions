// [SECTION] Arrays and Indexes
let grades = [98.5, 94.3, 89.2, 90.1];
let computer_brands = ["Acer", "Asus", "Lenovo", "Neo", "Redfox", "Gateway",
						"Toshiba", "Fujitsu"];
let mixed_array = [12, "Asus", null, undefined, {}];

// Alternative ways to write arrays
let my_tasks = [
	"drink hmtl",
	"eat javascript",
	"inhale CSS",
	"bake sass"
];

// Reassigning Values
console.log("Array before reassignment:");
console.log(my_tasks);

my_tasks[0] = "run hello world";
console.log("Array after reassignment:");
console.log(my_tasks);

// Reading from arrays
console.log(computer_brands[1]);
console.log(grades[3]);

// Getting the length of an array
console.log(computer_brands.length)

// Accessing the last element in an array
let index_of_last_element = computer_brands.length-1;
console.log(computer_brands[index_of_last_element]);
 

// Array Methods
// Push Method - Inserts at Last Value
let fruits = ["Apple", "Orange", "Kiwi", "Passionfruit"];

console.log("Current array:");
console.log(fruits);

fruits.push("Mango", "Cocomelon");
console.log("Updated array after push method:");
console.log(fruits);

// Pop Method - Removes Last Value
console.log("Current array:");
console.log(fruits);

let removed_item = fruits.pop();
console.log("Updated array after pop method:");
console.log(fruits);
console.log("Removed item: " + removed_item);

// Unshift Method - Inserts at first value
console.log("Current array:");
console.log(fruits);

fruits.unshift("Lime", "Star Apple");
console.log("Updated array after unshift method:");
console.log(fruits);

// Shift Method - Removes first value
console.log("Current array:");
console.log(fruits);

fruits.shift();
console.log("Updated array after shift method:");
console.log(fruits);

// Splice Method - Replaces the values of selected array index
console.log("Current array:");
console.log(fruits);

//1st value is the starting index, 2nd value is the length to be replaced
fruits.splice(0, 2, "Lime", "Cherry");
console.log("Updated array after splice method:");
console.log(fruits);

// Sort Method - Sorts a-z Alphabetically
console.log("Current array:");
console.log(fruits);

fruits.sort();
//fruits.reverse(); - Sorts in reverse order
console.log("Updated array after sort method:");
console.log(fruits);

//
let index_of_lenovo = computer_brands.indexOf("Lenovo");
console.log("The index of lenovo is: " + index_of_lenovo);

let index_of_lenovo_from_last_item = computer_brands.lastIndexOf("Lenovo");
console.log("The index of lenovo starting from the end of the array is: " 
	+ index_of_lenovo_from_last_item);

// Slice
let hobbies = ["Gaming", "Running", "Cheating", "Cycling", "Writing"];

let sliced_array_from_hobbies = hobbies.slice(1); // Only removes one from the start
console.log(sliced_array_from_hobbies);
console.log(hobbies);	

let sliced_array_from_hobbies_B = hobbies.slice(2, 4);
console.log(sliced_array_from_hobbies_B);
console.log(hobbies);	

let sliced_array_from_hobbies_C = hobbies.slice(-2); //-1 Starts from last
console.log(sliced_array_from_hobbies_C);
console.log(hobbies);	

// toString Method
let stringArray = hobbies.toString();
console.log(stringArray)

// concat Method - connect two different arrays
let greeting = ["hello", "world"];
let exclamation = ["!", "?"];

let concat_greeting = greeting.concat(exclamation);
console.log(concat_greeting);

// join method - converts array into a string
console.log(hobbies.join(' - '));

// foreach method
hobbies.forEach(function(hobby){
	console.log(hobby);
})

// map method
let numbers_list = [1, 2, 3, 4, 5];

let numbers_map = numbers_list.map(function(number){
	return number * 2;
})

console.log(numbers_list);
console.log(numbers_map);

// filter method - search functionality
let filtered_numbers = numbers_list.filter(function(number){
	return (number < 3);
})

console.log(filtered_numbers);

// Multi dimensional Array
let chessBoard = [
		    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
		    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
		    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
		    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
		    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
		    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
		    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
		    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];

console.log(chessBoard[1][4]);