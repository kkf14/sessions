const mongoose = require('mongoose');

const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: "pending"
	}
});

// Instead of doing the regular
// Task = mongoose.model("Task", taskSchema);
// We use module export to transfer it and finish the assign on a different file
module.exports = mongoose.model("Task", taskSchema);