const express = require('express');
const router = express.Router();
// Importing a Controller
const TaskController = require('../controllers/TaskController.js');

// Insert routes here
// Creating a new task
router.post('/', (request, response) => {
	// MINI ACTIVITY (15 mins.) until 2:25PM
	// -Use the proper function from the TaskController that will create a new task in the database.
	// -Make sure to pass the request body as an argument so the function will be able to use the 'name' property.
	// Type 'd' in GMeet once done.
	TaskController.createTask(request.body).then(result => {
		response.send(result); 
	})
})

// Get all tasks
router.get('/', (request, response) => {
	TaskController.getAllTasks().then(result => {
		response.send(result);
	})
})


// Get single task
router.get('/"id', (request, response) => {
	TaskController.getTask(request.params.id).then(result => {
		response.send(result);
	})
})


// Completing a task
// /complete is just a a label for the endpoint
router.put('/:id/complete', (request, response) => {
	TaskController.completeTask(request.params.id).then(result => {
		response.send(result);
	});
})
module.exports = router;