console.log("Hi");

// JSON Example
// {
// 	"city": "Pateros",
// 	"province": "Metro Manila",
// 	"country": "Philippines"
// }

// [SECTIONN] JSON Arrays
// "cities": [
// 	{
// 		"city": "Quezon City",
//  		"province": "Metro Manila",
//  		"country": "Philippines"
// 	},
// 	{
// 		"city": "Batangas City",
//  		"province": "Metro Manila",
//  		"country": "Philippines"
// 	},
// 	{
// 		"city": "Star City",
//  		"province": "Pasay",
//  		"country": "Philippines"
// 	}
// ]

// [Section] JSON METHODS

let zuitt_batches = [
{ batchName: "303" },
{ batchName: "303"}];

console.log("Output before stringification");
console.log(zuitt_batches);
console.log("Output after stringification");
console.log(JSON.stringify(zuitt_batches));

// User details
let first_name = "Kenji";
let last_name = "Fulcher";

// The stringify function/method converts JS Objects/Arrays into JSON String/format
let other_data = JSON.stringify({
	firstName: first_name,
	lastName: last_name
})

console.log(other_data);


// [SECTOIN] Convert Stringified JSON into Javascript Object/Arrays
let other_data_JSON = `[{"firstName": "Kenji", "lastName": "Fulcher"}]`;

// .parse converts the JSON String into a JS Object/Array
let parsed_other_data = JSON.parse(other_data_JSON);

console.log(parsed_other_data);

// It can now be accessed in regular javascript
console.log(parsed_other_data[0]);
