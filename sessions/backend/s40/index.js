// Server variables for initialization
const express = require('express'); // Imports express
const app = express(); // Initializes express
const port = 4000;

// Middleware
app.use(express.json()); // Registering middleware that will make express be able to read JSON format from requests
app.use(express.urlencoded({extended: true})); // A middleware that will allow express to be able to read data types other than the default string and array

// Server Listening
app.listen(port, () => console.log(`Server is running at ${port}`));

// [SECTION] Routes
app.get('/', (request, response) => {
	response.send("Hello World");
});

app.post('/greeting', (request, response) => {
	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}`)
});

// Mock Database
let users = [];

app.post('/register', (request,response) => {
	if(request.body.username !== '' && request.body.password !== ''){
		users.push(request.body);

		response.send(`User ${request.body.username} has successfully been registered!`);
	} else {
		response.send(`Please input BOTH username AND password.`)
	}
});  

app.get('/users', (request, response) => {
	response.send(users); // Returns an array of users
});

module.exports = app;