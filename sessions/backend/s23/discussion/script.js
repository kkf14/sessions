// Truthy values if(true){

if(true){
	console.log("Truthy");
}

if(1){
	console.log("Truthy");
}

if([]){
	console.log("Truthy");
}

// TERNARY OPERATORS

let result = (1 < 10) ? true : false;

console.log("Value returned from the ternary operator is " + result);

// Activity part temp
console.log("-----------------------");


function login(username, password, role) {
		
}

function checkAverage(num1, num2, num3, num4){
	average = (num1 + num2 + num3 + num4) / 4

	average	= Math.round(average);

	if (average <= 74)
		console.log("Hello, student, your average is " + average + ". The letter equivalent is F");
	else if (average >= 75 && average <= 79)
		console.log("Hello, student, your average is " + average + ". The letter equivalent is D");
	else if (average >= 80 && average <= 84)
		console.log("Hello, student, your average is " + average + ". The letter equivalent is C");
	else if (average >= 85 && average <= 89)
		console.log("Hello, student, your average is " + average + ". The letter equivalent is B");
	else if (average >= 90 && average <= 95)
		console.log("Hello, student, your average is " + average + ". The letter equivalent is A");
	else if (average >= 95)
		console.log("Hello, student, your average is " + average + ". The letter equivalent is A+");

}

checkAverage(97, 97, 97, 97);