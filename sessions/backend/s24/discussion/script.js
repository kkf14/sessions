// // SECTION While Loop

// let count = 5;

// while (count !== 0) {
// 	console.log("Current value of count: " + count);
// 	count--;
// };

// // SECTION Do-While Loop
// let number = Number(prompt("Give me a number"));

// do {
// 	console.log("Current value of number: " + number);

// 	number++;
// }while(number < 10)

// //SECTION For Loop
// for(let count = 0; count <= 20; count++){
// 	console.log("Current for loop value: " + count);
// }

let my_string = "Kenji";

console.log(my_string.length);

console.log(my_string[2]);

// for(let index = 0; index < my_string.length; index++){
// 	console.log(my_string[index]);
// }

// ---Mini Activity---
// let my_name = "Kenji Kevin Fulcher"

// for(let index = 0; index < my_name.length; index++){
// 	if(my_name[index] !== "a" && my_name[index] !== "e" && my_name[index] !== "i" && my_name[index] !== "o" && my_name[index] !== "u"){
// 		console.log(my_name[index]);
// 	}
// }

// let name_two = "rafael";

// for (let index = 0; index < name_two.length; index++){

// 	if(name_two[index] === "a"){
// 		console.log("Skipping...");
// 		continue;
// 	}

// 	if(name_two[index] === "e"){
// 		break;
// 	}

// 	console.log(name_two[index]);
// }


// Activity Part 1
let number = 100; //Number(prompt("Please input a number:"));
console.log(number);

for (let index = 100; index >= 0 ; index--){

	if(index <= 50){
	 	console.log("The value is at 50. Terminating the loop.");
	 	break;
	}

	if(index % 10 == 0){
		console.log("The number is divisible by 10. Skipping the number.");
		continue;
	}

	if(index % 5 == 0){
		console.log(index);
	}
}

// Activity Part 2
let string = "supercalifragilisticexpialidocious";

let filteredString = "";

for (let index = 0; index < string.length; index++){
	if (string[index] == "a" ||
		string[index] == "e" ||
		string[index] == "i" ||
		string[index] == "o" ||
		string[index] == "u"){
		continue;
	} else {
		filteredString = filteredString + string[index];
	}
}

console.log(string);
console.log(filteredString);