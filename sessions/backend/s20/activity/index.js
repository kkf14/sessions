/*
	1. Create a function named getUserInfo which is able to return an object. 

		The object returned should have the following properties:
		
		- key - data type

		- name - String
		- age -  Number
		- address - String
		- isMarried - Boolean
		- petName - String

		Note: Property names given is required and should not be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
*/

	function getUserInfo(){
		let key = {
			name: "Kenji",
 			age: 21,
 			address: "123 Street, Pasig City",
 			isMarried: false,
 			petName: "Java"
		};

		return key;
	}

	let userInfo = getUserInfo();
	// console.log(userInfo);

/*
	2. Create a function named getArtistsArray which is able to return an array with at least 5 names of your favorite bands or artists.
		
		- Note: the array returned should have at least 5 elements as strings.
			    function name given is required and cannot be changed.


		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
	
*/

	function getArtistsArray(){
		const artist_array = ["Mili", "Yorushika", "Chris Christodoulou", "AISHA", "Naoki Hoshimoto"];

		return artist_array;
	}

	let artistArray = getArtistsArray();
	// console.log(artistArray);	

/*
	3. Create a function named getSongsArray which is able to return an array with at least 5 titles of your favorite songs.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
*/

	function getSongsArray(){
		const songs_array = ["String Theocracy", "Itte", "The Rain Formerly Known as Purple", "Disaster of Passion", "Find Your One Way"];

		return songs_array;
	}

	let songsArray = getSongsArray();
	// console.log(songsArray);

/*
	4. Create a function named getMoviesArray which is able to return an array with at least 5 titles of your favorite movies.

		- Note: the array returned should have at least 5 elements as strings.
		        function name given is required and cannot be changed.

		To check, create a variable to save the value returned by the function.
		Then log the variable in the console.

		Note: This is optional.
*/

	function getMoviesArray(){
		const movies_array = ["Divergent", "El Camino", "Harry Potter", "Weathering With You", "Coach Carter"];

		return movies_array;
	}

	let moviesArray = getMoviesArray();
	// console.log(moviesArray);

/*
	5. Create a function named getPrimeNumberArray which is able to return an array with at least 5 prime numbers.

			- Note: the array returned should have numbers only.
			        function name given is required and cannot be changed.

			To check, create a variable to save the value returned by the function.
			Then log the variable in the console.

			Note: This is optional.
			
*/

	function getPrimeNumberArray(){
		const primenumber_array = [2, 3, 5, 7, 11];

		return primenumber_array;
	}

	let primenumber_array = getPrimeNumberArray();
	// console.log(primenumber_array);


//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
	module.exports = {

		getUserInfo: typeof getUserInfo !== 'undefined' ? getUserInfo : null,
		getArtistsArray: typeof getArtistsArray !== 'undefined' ? getArtistsArray : null,
		getSongsArray: typeof getSongsArray !== 'undefined' ? getSongsArray : null,
		getMoviesArray: typeof getMoviesArray !== 'undefined' ? getMoviesArray : null,
		getPrimeNumberArray: typeof getPrimeNumberArray !== 'undefined' ? getPrimeNumberArray : null,

	}
} catch(err){


}