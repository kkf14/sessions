// Function Declaration and Invocation
function printName(){
	console.log("My name is Jeff");
}

printName();

// Function Expression
let variable_function = function(){
	console.log("Hello from function expression!");
}

variable_function();

// Function Scoping
let global_variable = "Call me Mr. Worldwide";

console.log(global_variable);

function showNames(){
	let function_variable = "Joe";
	const function_const = "John";

	console.log(function_variable);
	console.log(function_const);
}

showNames();

// Nested Functions
function parentFunction(){
	let name = "Jane";

	function childFunction(){
		let nested_name = "John";

		console.log(name);

		console.log(nested_name);
	}

	childFunction();

	//console.log(nested_name); Error
}

parentFunction();

// childFunction(); // Error child is nested 

// BEST PRACTOCE FOR FUNCTION NAMING

function printWelcomeMessageForUser(){
	let first_name = prompt("Enter your first name: ");
	let last_name = prompt("Enter your last name: ");

	console.log("Hello, " + first_name + " " + last_name + "!");
	console.log("Welcome sa page ko!");
}

printWelcomeMessageForUser();

// RETURN STATEMENT

function fullName(){
	return "Earl Rafael Diaz";
}

console.log(fullName());