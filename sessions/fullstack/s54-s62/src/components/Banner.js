import {Button, Row, Col} from 'react-bootstrap';

export default function Banner(){
	return (
		<Row>
			<Col className="p-5 text-center">
				<h1>Kenji's Fishing Bootcamp</h1>
				<p>Fish for everyone!</p>
				<Button variant="primary">Enroll Now!</Button>
			</Col>
		</Row>
	)
}