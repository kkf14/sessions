import './App.css';
import AppNavbar from './components/AppNavbar.js';
import Home from './pages/Home.js';
import {Container} from 'react-bootstrap';
import Courses from './pages/Courses.js'


function App() {
  return (
    <>
      <AppNavbar/>

      <Container>
        <Home/>
        <Courses/>
      </Container>
    </>
  );
}

export default App;
